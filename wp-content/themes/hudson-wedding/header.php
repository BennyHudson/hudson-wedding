<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width"/>  
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php get_template_part('includes/include', 'favicon'); ?>
<title><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
</head>
<body <?php body_class('wedding'); ?>>

	<?php if(!is_front_page()) { ?>
		<section class="page-content">
			<section class="footer-fix">
				<header>
					<section class="container">
						<nav>
							<a href="<?php bloginfo('url'); ?>"><i class="fa fa-home"></i></a>
							<span>
								<a href="<?php echo get_the_permalink(10); ?>">Stay the Night</a>
								<a href="<?php bloginfo('url'); ?>/checkout">Checkout</a>
							</span>
						</nav>	
					</section>
				</header>
	<?php } ?>
