<?php
	function tastic_posts() {
		/*
			register_post_type(
				'testimonials', 
				array(
					'labels'		=> array (
										'name'					=> _x('Testimonials', 'post type general name'),
										'singular_name'			=> _x('Testimonial', 'post type singular name'),
										'add_new'				=> _x('Add new testimonial', 'book'),
										'add_new_item'			=> ('Add New Testimonial'),
										'edit_item'				=> ('Edit Testimonial'),
										'new_item'				=> ('New Testimonial'),
										'all_items'				=> ('All Testimonials'),
										'view_item'				=> ('View Testimonials'),
										'search_items'			=> ('Search Testimonials'),
										'not_found'				=> ('No Testimonials found'),
										'not_found_in_trash'	=> ('No Testimonials found in trash'),
										'parent_item_colon'		=> '',
										'menu_name'				=> 'Testimonials'
									),
					'description'	=> 'Holds all the Testimonials',
					'public'		=> true,
					'menu_position'	=> 19,
					'menu_icon'		=> 'dashicons-format-status',
					'supports'		=> array('title', 'editor', 'excerpt', 'revisions', 'thumbnail'),
					'has_archive'	=> true
				)
			);
		*/
	}
	function tastic_taxonomies() {
		/*
			register_taxonomy( 
				'publication-type', 
				'publications', 
				array(
		            'labels' 		=> array(
							 			'name'              => _x( 'Publication Type', 'taxonomy general name' ),
							            'singular_name'     => _x( 'Publication Type', 'taxonomy singular name' ),
							            'search_items'      => __( 'Search Publication Types' ),
							            'all_items'         => __( 'All Publication Types' ),
							            'parent_item'       => __( 'Parent Publication Type' ),
							            'parent_item_colon' => __( 'Parent Publication Type:' ),
							            'edit_item'         => __( 'Edit Publication Type' ), 
							            'update_item'       => __( 'Update Publication Type' ),
							            'add_new_item'      => __( 'Add New Publication Type' ),
							            'new_item_name'     => __( 'New Publication Type' ),
							            'menu_name'         => __( 'Publication Types' ),
							        ),
		            'hierarchical' => true,
		        )
			);
		*/
    }
?>
