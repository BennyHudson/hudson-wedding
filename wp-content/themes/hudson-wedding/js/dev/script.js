$(document).ready(function() {

    var windowHeight = $(window).height();
    var headerHeight = $('header').outerHeight();
    var footerHeight = $('footer').outerHeight();

    $('.footer-fix').css({
        'padding-top' : headerHeight,
        'padding-bottom' : footerHeight
    });

    jQuery('.parallax-window').parallax(); 

	jQuery('.container').fitVids();

	var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);

    jQuery('.slider').bxSlider({
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>'
    });

    $('.quantity').prepend('<label>People:</label>');
    $('.summary .price').html('Friday 2nd June 2017');

    $('#order_review_heading, #order_review table').wrapAll('<div class="col-3" />');
    //$('#payment').wrapAll('<div class="col-4" />');
    //$('.col-3').prepend('<h3>Payment</h3>').insertAfter('.col-3');

    $('#order_review_heading').html('Confirmation');
    $('.col-3').append('<a href="https://hudsonwedding.co.uk/basket" class="button small">Change</a><a href="#" class="button payment-trigger">Continue</a>');

    $('.payment-trigger').click(function(e) {
        e.preventDefault();
        $('.col-3').addClass('active');
        $('#payment').slideDown();
        $('html, body').animate({
            scrollTop: $('#payment').offset().top - headerHeight
        }, 1000);
    });

});

$(window).resize(function() {

    // Global variables

        var windowHeight = $(window).height();
        var navHeight = $('.scrolling-nav').outerHeight();

    // Fix filters on scroll

        if($(this).scrollTop() > (windowHeight - navHeight)) { 
            $('.scrolling-nav').addClass('fixed');
        } else {
            $('.scrolling-nav').removeClass('fixed');
        }

});

$(window).scroll(function() {

    // Global variables

        var windowHeight = $(window).height();
        var navHeight = $('.scrolling-nav').outerHeight();
        var scrollTop = $(this).scrollTop();

    // Fix filters on scroll

        if($(this).scrollTop() > (windowHeight - navHeight)) { 
            $('.scrolling-nav').addClass('fixed');
        } else {
            $('.scrolling-nav').removeClass('fixed');
            $('nav a.active').removeClass('active');
        } 

    // Change active state on scroll

    $('.scrolling-section').each(function() {

        var topDistance = $(this).offset().top;

        if ( (topDistance - ( navHeight + 10 )) < scrollTop ) {
            var activeSection = $(this).attr('id');
            
            $('nav a.active').removeClass('active');
            $('.target-' + activeSection).addClass('active');  
        }
    });  

});

enquire
    .register("screen and (min-width:65em)", function() { 
        $(document).ready(function() {
            var navHeight = $('.scrolling-nav').outerHeight();

            // Scrolling Nav

            $('.page-scroller').click(function(e) {
                e.preventDefault();

                var target = $(this).data('target');

                $('html, body').animate({
                    scrollTop: $('#' + target).offset().top - navHeight
                }, 1000);
            });
        });
    }, true)
    .register("screen and (max-width: 31em) and (orientation: portrait)", function() { 
        $(document).ready(function() {
            
        });
    })
    .register("screen and (max-width: 65em) and (orientation: portrait)", function() { 
        $(document).ready(function() {

            // Scrolling Nav

            $('.page-scroller').click(function(e) {
                e.preventDefault();

                var target = $(this).data('target');

                $('html, body').animate({
                    scrollTop: $('#' + target).offset().top
                }, 1000);
            });
             
        });
    })
    .register("screen and (max-width: 65em)", function() { 
        $(document).ready(function() {
            
        });
    })

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
