<?php
	 
	add_action( 'after_setup_theme', 'tastic_theme_setup' );
	function tastic_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'tastic_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		add_action( 'login_enqueue_scripts', 'my_login_logo' );
		add_action('login_head', 'add_favicon');
		add_action('admin_head', 'add_favicon');
		add_action('init', 'tastic_posts');
		add_action('init', 'tastic_taxonomies');
		add_action('init', 'flush_rewrite_rules');
		add_action( 'wp_enqueue_scripts', 'tastic_scripts' );
		add_theme_support( 'woocommerce' );
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
		add_action( 'admin_init', 'tastic_users' );
		add_filter('admin_footer_text', 'tastic_footer');
		add_action('wp_dashboard_setup', 'tastic_dashboard');
		add_action('admin_head', 'tastic_admin');
		add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
		add_filter( 'wpseo_metabox_prio', function() { return 'low';});
		add_image_size( 'gallery-thumb', 300, 240, true );
		add_image_size( 'post-feature', 900, 450, true );
	}

	get_template_part('functions/include', 'favicons');
	get_template_part('functions/include', 'postnav');
	get_template_part('functions/include', 'menus');
	get_template_part('functions/include', 'scripts');
	get_template_part('functions/include', 'sidebar');
	get_template_part('functions/include', 'cpts');
	get_template_part('functions/include', 'users');
	get_template_part('functions/include', 'email');
	get_template_part('functions/include', 'acf');
	get_template_part('functions/include', 'footer');
	get_template_part('functions/include', 'welcome');
	get_template_part('functions/include', 'adminstyle');
	get_template_part('functions/include', 'gallery'); 

	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

	add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' ); 

	function woo_custom_order_button_text() {
	    return __( 'Pay Now', 'woocommerce' ); 
	}
	function woocommerce_button_proceed_to_checkout() {
       $checkout_url = WC()->cart->get_checkout_url();
       ?>
       <a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Continue', 'woocommerce' ); ?></a>
       <?php
    }

    /**
	 * Custom text on the receipt page.
	 */
	function isa_order_received_text( $text, $order ) {
	    $new = "<span>Thank you.</span>Your booking has been made and you'll receive a confirmation email shortly.";
	    return $new;
	}
	add_filter('woocommerce_thankyou_order_received_text', 'isa_order_received_text', 10, 2 );


	add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

	// Our hooked in function - $fields is passed via the filter!
	function custom_override_checkout_fields( $fields ) {
	     $fields['order']['order_comments']['required'] = true;
	     return $fields;
	}

	add_filter( 'woocommerce_email_customer_details_fields', 'woocommerce_email_customer_details_fields_remove_note_text' );

	function woocommerce_email_customer_details_fields_remove_note_text( $meta ) {
		unset( $meta['customer_note'] );
		return $meta;
	}

	/**
	 * Auto Complete all WooCommerce orders.
	 */
	add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
	function custom_woocommerce_auto_complete_order( $order_id ) { 
	    if ( ! $order_id ) {
	        return;
	    }

	    $order = wc_get_order( $order_id );
	    $order->update_status( 'completed' );
	}

?>
