				<footer>
					<?php if(is_front_page()) { ?>
						<div class="divider"></div>
					<?php } ?>
					<a href="http://ben-hudson.co.uk" target="_blank">Ben built this website</a>
				</footer>
	<?php if(!is_front_page()) { ?>
			</section>
		</section>
	<?php } ?>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBR5bX6m_CEPwitun65XjrFWYZVRtzqADA"></script>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-35663434-7', 'auto');
	  ga('send', 'pageview');

	</script>

	<?php wp_footer(); ?>
</body>
</html>
