<?php
	//Template Name: Landing Page
	get_header();
?>

	<section class="landing-flood parallax-window" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/images/landing-feature.jpg" data-natural-width="1920" data-natural-height="1080" data-speed="0.5">

		<section class="container ultra">

			<div>

				<h1><?php bloginfo('name'); ?></h1>
				<h2>Friday 2nd June 2017</h2>

				<a href="#" class="page-scroller tablet-scroller" data-target="party"><i class="fa fa-angle-down"></i></a>

			</div>

		</section>

	</section>

	<nav class="scrolling-nav">
		<section class="container">
			<ul>
				<!--<li><a href="#" class="page-scroller target-details" data-target="details">The Details</a></li>-->
				<li><a href="#" class="page-scroller target-party" data-target="party">The Wedding Party</a></li>
				<li><a href="#" class="page-scroller target-day" data-target="day">The Day</a></li>
				<li><a href="#" class="page-scroller target-venue" data-target="venue">The Venue</a></li>
				<li><a href="#" class="page-scroller target-gifts" data-target="gifts">Gifts</a></li>
				<li><a href="#" class="page-scroller target-rsvp" data-target="rsvp">RSVP</a></li>
			</ul>
		</section>
	</nav>

	<section class="landing-main">

	<!--
		<section class="scrolling-section" id="details">
			<section class="container ultra">
				<h2 class="feature-title">Friday 2nd June 2017</h2>
				<h2 class="feature-title alt">Froyle Park Country Estate</h2>
				<h3 class="feature-title">Ryebridge Lane, Upper Froyle, Alton, Hampshire, GU34 4LA</h3>
			</section>
		</section>	

		<div class="divider"></div>

	-->

		<section class="scrolling-section" id="party">
			<section class="container ultra">

				<h2 class="feature-title">The Wedding Party</h2>

				<ul class="photo-list two-wide">
					<li>
						<div class="circle-clip" id="ak"></div>
						<h2>
							Aimee Keith
							<span>The Bride</span>
						</h2>
					</li>
					<li>
						<div class="circle-clip" id="bh"></div>
						<h2>
							Ben Hudson
							<span>The Groom</span>
						</h2>
					</li>
				</ul>
				<ul class="photo-list four-wide">
					<li>
						<div class="circle-clip" id="mc"></div>
						<h2>
							Melanie Collins
							<span>Maid of Honour</span>
						</h2>
					</li>
					<li>
						<div class="circle-clip" id="kk"></div>
						<h2>
							Katie Keith
							<span>Bridesmaid</span>
						</h2>
					</li>
					<li>
						<div class="circle-clip" id="ch"></div>
						<h2>
							Chloe Hudson
							<span>Bridesmaid</span>
						</h2>
					</li>
					<li>
						<div class="circle-clip" id="dk"></div>
						<h2>
							Daniel Kite
							<span>Best Man</span>
						</h2>
					</li>
				</ul>

			</section>
		</section>	

	</section>

	<section class="scrolling-section the-day" id="day">
		<section class="container ultra">
			<h2 class="feature-title">The Day</h2>
			<ul class="circle-list five-wide">
				<li>
					<div class="circle-clip taxi"></div>
					<h3>13:30 <span>Guests <br>Arrive</span></h3>
				</li>
				<li>
					<div class="circle-clip rings"></div>
					<h3>14:00 <span>Ceremony Begins</span></h3>
				</li>
				<li>
					<div class="circle-clip glasses"></div>
					<h3>14:30 <span>Drinks Reception</span></h3>
				</li>
				<li>
					<div class="circle-clip cutlery"></div>
					<h3>16:30 <span>Wedding Breakfast</span></h3>
				</li>
				<li>
					<div class="circle-clip balloons"></div>
					<h3>19:30 <span>Evening <br>Guests Arrive</span></h3>
				</li>
				<li>
					<div class="circle-clip taxi"></div>
					<h3>00:00 <span>Carriages</span></h3>
				</li>
			</ul>
		</section>	
	</section>

	<section class="scrolling-section landing-venue" id="venue">
		<section class="container ultra narrow">
			<h2 class="feature-title">The Venue</h2>
			<h2 class="alt">Froyle Park Country Estate</h2>
			<h3 class="feature-title">Ryebridge Lane, Upper Froyle, Alton, Hampshire, GU34 4LA</h3>
			<p>Located between Farnham and Alton. Using the A31, turn off at the Hen and Chicken public house and proceed towards Upper Froyle, the venue is on the right hand side just before St Marys Church.</p>
		</section>
		<section class="container ultra no-top">
			<aside class="slider-half">
				<h2 class="pink">Hotels</h2>
				<div class="slider-container">
					<ul class="slider">
						<li>
							<span class="pink">The Anchor Inn <em>(1.6 miles away)</em></span>
							<span>Lower Froyle, Alton, Hants GU34 4NA</span>
							<span>01420 23261</span>
						</li>
						<li>
							<span class="pink">Upper Neatham Mill Farm <em>(3.1 miles away)</em></span>
							<span>Holybourne, Alton, Hants GU34 4EP</span>
							<span>01420 542908</span>
						</li>
						<li>
							<span class="pink">Alton House Hotel <em>(3.6 miles away)</em></span>
							<span>57 Normandy Street, Alton, Hants GU34 1DW</span>
							<span>01420 80033</span>
						</li>
						<li>
							<span class="pink">The Bishops Table Hotel <em>(6.7 miles away)</em></span>
							<span>27 West Street, Farnham, Surrey GU9 7DR</span>
							<span>01252 710 222</span>
						</li>
						<li>
							<span class="pink">The George Hotel <em>(6.9 miles away)</em></span>
							<span>High Street, Odiham, Hants RG29 1LP</span>
							<span>01256 702081</span>
						</li>
						<li>
							<span class="pink">The Mercure Bush Hotel <em>(7 miles away)</em></span>
							<span>The Borough, Farnham, Surrey GU9 7NN</span>
							<span>01252 715237</span>
						</li>
						<li>
							<span class="pink">The Princess Royal Inn <em>(9.3 miles away)</em></span>
							<span>Runfold, Nr Farnham, Surrey GU10 1NX</span>
							<span>01252 782243</span>
						</li>
						<li>
							<span class="pink">The Hog’s Back Hotel <em>(10.4 miles away)</em></span>
							<span>Seale, Farnham, Surrey GU10 1EX</span>
							<span>01252 782 345</span>
						</li>
					</ul>
				</div>
			</aside>
			<aside class="slider-half">
				<h2 class="pink">Carriages</h2>
				<div class="slider-container">
					<ul class="slider">
						<li>
							<span class="pink">Wilson Taxis</span>
							<span>Alton</span>
							<span>01420 87777</span>
						</li>
						<li>
							<span class="pink">Taj Taxis</span>
							<span>Alton</span>
							<span>01420 544544</span>
						</li>
						<li>
							<span class="pink">Alton 8</span>
							<span>Alton</span>
							<span>01420 88888</span>
						</li>
						<li>
							<span class="pink">SQ Taxis</span>
							<span>Farnham</span>
							<span>07591 282414 / 07926 751903</span>
						</li>
						<li>
							<span class="pink">Courtesey Cars</span>
							<span>Farnham</span>
							<span>01252 711113</span>
						</li>
						<li>
							<span class="pink">Farnham Taxis</span>
							<span>Farnham</span>
							<span>01252 715555</span>
						</li>
						<li>
							<span class="pink">Sapphire Taxis</span>
							<span>Farnham</span>
							<span>01252 717100</span>
						</li>
						<li>
							<span class="pink">Accord Taxi</span>
							<span>Farnham</span>
							<span>01252 721130</span>
						</li>
					</ul>
				</div>
			</aside>
		</section>
	</section>

	<section class="venue-flood parallax-window" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/images/venue-feature.jpg" data-natural-width="1920" data-natural-height="1080" data-speed="0.5">

	</section>

	<section class="scrolling-section gifts" id="gifts">
		<section class="container ultra narrow">
			<h2 class="feature-title">Gifts</h2>
			<p>We don't need any gifts, so please don't feel obliged. If you would like to give us something, we'd really appreciate a contribution towards our honeymoon.</p>
			<p><i class="fa fa-smile-o pink" aria-hidden="true"></i></p>
		</section>
	</section>

	<div class="divider"></div>

	<section class="scrolling-section rsvp" id="rsvp">
		<section class="container ultra">
			<section class="form-box">
				<h2 class="feature-title pink">Your RSVP</h2>
				<?php echo do_shortcode('[contact-form-7 id="4" title="RSVP"]'); ?>
			</section>
			<p class="rsvp-note">To give all our guests the opportunity to let their hair down and have a good time, we've decided to keep our wedding an adults only occasion.</p>
			<p class="rsvp-note">If you would prefer to send a postal RSVP, our address is: <br>21 Runnymede Drive, Odiham Hampshire, RG29 1FP</p>
		</section>
	</section>

<?php get_footer(); ?>
