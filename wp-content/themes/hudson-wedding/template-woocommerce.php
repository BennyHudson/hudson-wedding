<?php 
	// Template Name: WooCommerce
	get_header(); 
?>

	<?php if(is_page(7)) { ?>
		<section class="container ultra">
			<h1 class="page-title white center">Reservation Details</h1>
	<?php } else { ?>
		<section class="container ultra narrow">
	<?php } ?>
	<?php the_content(); ?>
	</section>

<?php get_footer(); ?>
